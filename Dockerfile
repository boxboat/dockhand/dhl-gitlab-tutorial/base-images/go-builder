FROM golang:1.19-bullseye

RUN apt-get update \
    && apt-get install -y \
        git \
        bash \
        build-essential \
        curl \
    && rm -rf /var/lib/apt/lists/*
